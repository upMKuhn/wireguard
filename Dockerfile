FROM ubuntu:20.04

ENV WG_INTERFACE wg0
RUN apt update && apt upgrade -y
RUN apt install -y wireguard iproute2

RUN mkdir -p /app
WORKDIR /app

COPY ./provision/* /app/

ENTRYPOINT [ "/app/entrypoint.sh" ]