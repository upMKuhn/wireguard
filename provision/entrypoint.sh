#! /bin/bash

if [ -z $WG_INTERFACE ]; then 
    export WG_INTERFACE=wg0
fi

echo Starting interface $WG_INTERFACE 
cp ./wg0.conf /etc/wireguard/wg0.conf
wg-quick up $WG_INTERFACE